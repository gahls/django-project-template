{{ project_name }}
==================

Get the the development environment up and running:

    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    python manage.py syncdb --migrate

and run the development server:

    python manage.py runserver