import getpass

from fabric.api import task, env, run, sudo, cd, prefix, put
from fabric.contrib.files import exists
from fabric.colors import yellow, green, blue


# SETUP DEPLOYMENT ENV
# --------------------

@task
def prod():
    env.hosts = '0.0.0.0'   # Enter production server host
    env.user = 'deploy'     # Enter production deployment user

    env['settings'] = {
        'project_path': '/webapps/{{ project_name }}',
        'application_name': '{{ project_name }}',
        'settings_module': 'production'
    }

    prompt_for_sudo_password()

@task
def test():
    env.hosts = '0.0.0.0'   # Enter test server host
    env.user = 'deploy'     # Enter test ser ver deployment user

    env['settings'] = {
        'project_path': '/webapps/{{ project_name }}',
        'application_name': '{{ project_name }}',
        'settings_module': 'test'
    }

def prompt_for_sudo_password():
    env.password = getpass.getpass(green('Enter sudo password for deploy user: '))


# MULTI-TASK UTILS
# ----------------

@task
def initialize():
    repo_url = raw_input(green('Enter repo URL: '))
    db_password = getpass.getpass(green('Enter database password: '))

    if len(repo_url):
        run('mkdir %s' % env.settings['project_path'])

        with cd(env.settings['project_path']):
            run('mkdir logs')
            run('virtualenv env')
            run('git clone %s ./' % repo_url)

            # Add env variables
            run('echo "export DATABASE_PASSWORD="%s" >> env/bin/activate' % db_password)
            run('echo "export DJANGO_SETTINGS_MODULE="%s.settings.%s" >> env/bin/activate' % (
                    env.settings['application_name'],
                    env.settings['settings_module']
                ))

            deploy_config()
            deploy(restart_process=False)

            # Make sure start_app script is executable
            run('chmod +x start_app.sh')

            # Start supervisor process for the first time
            sudo('supervisorctl start %s' % env.settings['application_name'])

@task
def deploy(restart_process=True):
    pull_latest()
    install_requirements()
    collect_static()
    sync_migrate_db()

    if restart_process:
        restart_service()

@task
def deploy_config():
    upload_supervisor_config()
    upload_nginx_config()
    reload_supervisor_config()
    reload_nginx_config()

@task
def shell():
    with cd(env.settings['project_path']):
        with prefix('source env/bin/activate'):
            run('python manage.py shell')


# SINGLE TASK UTILS
# -----------------

@task
def pull_latest():
    with cd(env.settings['project_path']):
        run('git pull')

@task
def checkout_commit(commit):
    with cd(env.settings['project_path']):
        run('git checkout %s' % commit)

@task
def install_requirements():
    with cd(env.settings['project_path']):
        with prefix('source env/bin/activate'):
            run('pip install -r requirements.txt')

@task
def collect_static():
    with cd(env.settings['project_path']):
        with prefix('source env/bin/activate'):
            run('python manage.py collectstatic --noinput')

@task
def sync_migrate_db():
    with cd(env.settings['project_path']):
        with prefix('source env/bin/activate'):
            run('python manage.py syncdb --migrate')

@task
def restart_service():
    sudo('supervisorctl restart %s' % env.settings['application_name'])

@task
def reload_nginx_config():
    sudo('/etc/init.d/nginx reload')

@task
def reload_supervisor_config():
    sudo('supervisorctl reread')
    sudo('supervisorctl update')

@task
def monitor():
    sudo('tail -f /var/log/nginx/error.log -f %s/logs/*' % env.settings['project_path'])

# CONFIG TASKS
# ------------

@task
def upload_supervisor_config():
    put(
        'config/{{ project_name }}.supervisor.conf',
        '/etc/supervisor/conf.d/',
        use_sudo=True
    )

@task
def upload_nginx_config():
    put(
        'config/{{ project_name }}.nginx.conf',
        '/etc/nginx/sites-available/',
        use_sudo=True
    )

    if not exists('/etc/nginx/sites-enabled/{{ project_name }}.nginx.conf'):
        with cd('/etc/nginx/sites-enabled'):
            sudo('ln -s ../sites-available/{{ project_name }}.nginx.conf')
