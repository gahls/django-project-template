import os

from .base import *

DEBUG = False

ALLOWED_HOSTS = []

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ project_name }}',
        'USER': '{{ project_name }}',
        'PASSWORD': os.environ['DATABASE_PASSWORD'],
        'HOST': '127.0.0.1'
    }
}

STATIC_ROOT = os.join(BASE_DIR, '../static')
MEDIA_ROOT = os.join(BASE_DIR, '../media')