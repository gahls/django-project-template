#!/bin/bash

NAME="{{ project_name }}"                                           # App name
DJANGODIR=/webapps/{{ project_name }}                               # Django project path
SOCKFILE=/webapps/{{ project_name }}/run/gunicorn.socket            # Unix socket
USER=deploy                                                         # User to run the process
GROUP=deploy                                                        # User group to run the process
NUM_WORKERS=3                                                       # The number of workers
DJANGO_WSGI_MODULE={{ project_name }}.wsgi                          # WSGI module

cd $DJANGODIR
source env/bin/activate
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

RUNDIR=$(dirname $SOCKFILE)

test -d $RUNDIR || mkdir -p $RUNDIR

exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --log-level=debug \
  --bind=unix:$SOCKFILE